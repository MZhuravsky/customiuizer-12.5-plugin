package ru.mike.customiuizer125plugin.hooks

import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers
import de.robv.android.xposed.callbacks.XC_LoadPackage
import ru.mike.customiuizer125plugin.utils.*

object Views {

    fun fixQsFooterHeight(lpp: XC_LoadPackage.LoadPackageParam) {
        lpp.find("com.android.systemui.qs.MiuiQSContainer")?.declaredMethods?.firstOrNull { it.name == "addQSContent" }?.let { m ->
            XposedBridge.hookMethod(m, object : Helpers.MethodHook() {
                override fun after(param: XC_MethodHook.MethodHookParam) {
                    param.thisObject.getFromFieldSafe<ViewGroup>("footer")?.let { v ->
                        val lp = v.layoutParams as? ViewGroup.MarginLayoutParams ?: return
                        val h = lp.height
                        val scale = if (v.childCount > 0 && v.getChildAt(0).isVisible) 0.5f else 0.1f
                        lp.height = (h * scale).toInt()
                        v.layoutParams = lp
                    }
                }
            })
        }
    }

    fun changeBatteryBatteryTextSizeHook(lpp: XC_LoadPackage.LoadPackageParam) {
        fun increaseBatteryTextSize(textView: TextView, resetSaved: Boolean = false) {
            val textScale = 1.15f
            val currTextSize: Float = if (resetSaved)
                textView.textSize * textScale
            else XposedHelpers.getAdditionalInstanceField(textView, "savedTextSize") as? Float ?: textView.textSize * textScale
            XposedHelpers.setAdditionalInstanceField(textView, "savedTextSize", currTextSize)
            if (textView.textSize != currTextSize) {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, currTextSize)
            }

            val parent = textView.parent as? LinearLayout ?: return
            if (parent.paddingStart == 0) {
                parent.setPaddingRelative(ViewUtils.dpToPx(parent.context, 4), parent.paddingTop, parent.paddingEnd, parent.paddingBottom)
                /*(textView.layoutParams as? ViewGroup.MarginLayoutParams)?.let {
                    it.bottomMargin = ViewUtils.dpToPx(parent.context, 1)
                    textView.layoutParams = it
                }*/
            }
        }

        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "updateChargingIconView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                param.thisObject.getFromFieldsSafe<TextView>("mBatteryTextDigitView", "mBatteryPercentView").forEach {
                    increaseBatteryTextSize(it)
                }
            }
        })
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "onDensityOrFontScaleChanged", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                param.thisObject.getFromFieldsSafe<TextView>("mBatteryTextDigitView", "mBatteryPercentView").forEach {
                    increaseBatteryTextSize(it, true)
                }
            }
        })
    }
}