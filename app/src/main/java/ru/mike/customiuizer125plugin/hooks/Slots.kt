package ru.mike.customiuizer125plugin.hooks

import de.robv.android.xposed.XposedBridge
import ru.mike.customiuizer125plugin.XposedMod
import ru.mike.customiuizer125plugin.utils.Logs
import ru.mike.customiuizer125plugin.utils.getBooleanPref

object Slots {

    var hide_signal = false
    var hide_nosims = false
    var hide_wifi = false
    var hide_hotspot = false
    var hide_volte = false
    var hide_vowifi = false
    var hide_headset = false
    var hide_volume = false
    var hide_quiet = false
    var hide_mute = false
    var hide_speakerphone = false
    var hide_call_record = false
    var hide_alarm_clock = false
    var hide_managed_profile = false

    fun checkNeedHideSlots(): Boolean {
        hide_signal = XposedMod.prefs.getBooleanPref("system_statusbaricons_signal")
        hide_nosims = XposedMod.prefs.getBooleanPref("system_statusbaricons_nosims")
        hide_wifi = XposedMod.prefs.getBooleanPref("system_statusbaricons_wifi")
        hide_hotspot = XposedMod.prefs.getBooleanPref("system_statusbaricons_hotspot")
        hide_volte = XposedMod.prefs.getBooleanPref("system_statusbaricons_volte")
        hide_vowifi = XposedMod.prefs.getBooleanPref("system_statusbaricons_vowifi")
        hide_headset = XposedMod.prefs.getBooleanPref("system_statusbaricons_headset")
        hide_volume = XposedMod.prefs.getBooleanPref("system_statusbaricons_sound")
        hide_quiet = XposedMod.prefs.getBooleanPref("system_statusbaricons_dnd")
        hide_mute = XposedMod.prefs.getBooleanPref("system_statusbaricons_mute")
        hide_speakerphone = XposedMod.prefs.getBooleanPref("system_statusbaricons_speaker")
        hide_call_record = XposedMod.prefs.getBooleanPref("system_statusbaricons_record")
        hide_alarm_clock = XposedMod.prefs.getBooleanPref("system_statusbaricons_alarm")
        hide_managed_profile = XposedMod.prefs.getBooleanPref("system_statusbaricons_profile")

        return hide_signal || hide_nosims || hide_wifi || hide_hotspot || hide_volte || hide_vowifi
                || hide_headset || hide_volume || hide_quiet || hide_mute || hide_speakerphone
                || hide_call_record || hide_alarm_clock || hide_managed_profile
    }

    fun checkSlot(slotName: String): Boolean {
        return try {
            "mobile" == slotName && hide_signal
                    // || "demo_mobile" == slotName &&  hide_signal
                    || "no_sim" == slotName && hide_nosims
                    || "wifi" == slotName && hide_wifi
                    // || "slave_wifi" == slotName && hide_wifi
                    // || "demo_wifi" == slotName && hide_wifi
                    || "hotspot" == slotName && hide_hotspot

                    // hide_volte
                    // hide_vowifi

                    || "headset" == slotName && hide_headset
                    || "volume" == slotName && hide_volume
                    || "quiet" == slotName && hide_quiet
                    || "mute" == slotName && hide_mute
                    || "speakerphone" == slotName && hide_speakerphone
                    || "call_record" == slotName && hide_call_record
                    || "alarm_clock" == slotName && hide_alarm_clock
                    || "managed_profile" == slotName && hide_managed_profile
        } catch (t: Throwable) {
            Logs.x(t)
            false
        }
    }

}