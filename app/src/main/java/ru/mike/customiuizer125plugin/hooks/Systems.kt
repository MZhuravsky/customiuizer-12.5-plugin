package ru.mike.customiuizer125plugin.hooks

import android.annotation.SuppressLint
import android.app.Notification
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Typeface
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.Looper
import android.os.PowerManager
import android.os.SystemClock
import android.provider.Settings
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import de.robv.android.xposed.XC_MethodReplacement
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam
import de.robv.android.xposed.callbacks.XCallback
import ru.mike.customiuizer125plugin.XposedMod
import ru.mike.customiuizer125plugin.hooks.Systems.luxRef
import ru.mike.customiuizer125plugin.utils.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.math.roundToInt

object Systems {

    fun hideIconsBattery1Hook(lpp: LoadPackageParam) {
        fun handleView(view: View?) {
            view?.let { v ->
                (v.layoutParams as? MarginLayoutParams)?.let { lp ->
                    val w = lp.width
                    if (w != 0) {
                        XposedHelpers.setAdditionalInstanceField(v, "savedWidth", w)
                        lp.width = 0
                        v.layoutParams = lp
                    }
                }
                v.visibility = View.GONE
            }
        }

        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "updateChargingIconView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                handleView(param.thisObject.getFromFieldSafe<View>("mBatteryIconView"))
            }
        })
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "initMiuiView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                handleView(param.thisObject.getFromFieldSafe<View>("mBatteryIconView"))
            }
        })
    }

    fun hideIconsBattery2Hook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "updateChargingIconView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                (param.thisObject.getFromFieldSafe("mBatteryTextDigitView") as? View)?.visibility = View.GONE
            }
        })
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "initMiuiView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                (param.thisObject.getFromFieldSafe("mBatteryTextDigitView") as? View)?.visibility = View.GONE
            }
        })
    }

    fun hideIconsBattery3Hook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "updateChargingIconView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                (param.thisObject.getFromFieldSafe("mBatteryChargingView") as? View)?.visibility = View.GONE
                (param.thisObject.getFromFieldSafe("mBatteryChargingInView") as? View)?.visibility = View.GONE
            }
        })
        lpp.findAndHook("com.android.systemui.MiuiBatteryMeterView", "initMiuiView", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                (param.thisObject.getFromFieldSafe("mBatteryChargingView") as? View)?.visibility = View.GONE
                (param.thisObject.getFromFieldSafe("mBatteryChargingInView") as? View)?.visibility = View.GONE
            }
        })
    }

    fun hideIconsBluetoothHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.phone.MiuiPhoneStatusBarPolicy", "updateBluetooth", String::class.java, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val opt: Int = XposedMod.prefs.getStringAsIntPref("system_statusbaricons_bluetooth", 1)
                val isBluetoothConnected = XposedHelpers.callMethod(XposedHelpers.getObjectField(param.thisObject, "mBluetooth"), "isBluetoothConnected") as Boolean
                if (opt == 3 || opt == 2 && !isBluetoothConnected) {
                    param.thisObject.getFromFieldSafe<Any>("mIconController")?.let { iconController ->
                        XposedHelpers.callMethod(iconController, "setIconVisibility", "bluetooth", false)
                    }
                }
            }
        })
    }

    fun showNotificationsAfterUnlockHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.notification.ExpandedNotification", "hasShownAfterUnlock", XC_MethodReplacement.returnConstant(false))
        lpp.findAndHook("com.android.systemui.statusbar.notification.ExpandedNotification", "setHasShownAfterUnlock", Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                XposedHelpers.setBooleanField(param.thisObject, "mHasShownAfterUnlock", false)
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.notification.MiuiNotificationCompat", "isKeptOnKeyguard", Notification::class.java, XC_MethodReplacement.returnConstant(true))
    }

    fun hideIconsVPNHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.policy.SecurityControllerImpl", "isSilentVpnPackage", object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                param.result = true
            }
        })
    }

    fun hideSlotIconsHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.phone.StatusBarIconControllerImpl", "setIconVisibility", String::class.java, Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                if (Slots.checkSlot(param.args[0] as String))
                    param.args[1] = false
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.phone.MiuiDripLeftStatusBarIconControllerImpl", "setIconVisibility", String::class.java, Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                if (Slots.checkSlot(param.args[0] as String))
                    param.args[1] = false
            }
        })
    }

    /*fun notificationVolumeServiceHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.server.audio.AudioService", "updateStreamVolumeAlias", Boolean::class.javaPrimitiveType, String::class.java, String::class.java, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mStreamVolumeAlias = XposedHelpers.getObjectField(param.thisObject, "mStreamVolumeAlias") as IntArray
                mStreamVolumeAlias[1] = 1
                mStreamVolumeAlias[5] = 5
                XposedHelpers.setObjectField(param.thisObject, "mStreamVolumeAlias", mStreamVolumeAlias)
            }
        })
        lpp.findAndHook("com.android.server.audio.AudioService\$VolumeStreamState", "readSettings", object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                val mStreamType = XposedHelpers.getIntField(param.thisObject, "mStreamType")
                if (mStreamType != 1) return
                synchronized(param.method.declaringClass) {
                    val audioSystem = XposedHelpers.findClass("android.media.AudioSystem", null)
                    // val DEVICE_OUT_ALL = XposedHelpers.getStaticIntField(audioSystem, "DEVICE_OUT_ALL")
                    val DEVICE_OUT_ALL_SET = XposedHelpers.getStaticObjectField(audioSystem, "DEVICE_OUT_ALL_SET") as? Set<Int> ?: return
                    var DEVICE_OUT_ALL: Int = DEVICE_OUT_ALL_SET.first()
                    DEVICE_OUT_ALL_SET.drop(1).forEach { DEVICE_OUT_ALL = DEVICE_OUT_ALL or it }
                    val DEVICE_OUT_DEFAULT = XposedHelpers.getStaticIntField(audioSystem, "DEVICE_OUT_DEFAULT")
                    val DEFAULT_STREAM_VOLUME = XposedHelpers.getStaticObjectField(audioSystem, "DEFAULT_STREAM_VOLUME") as IntArray
                    var remainingDevices = DEVICE_OUT_ALL
                    var i = 0
                    while (remainingDevices != 0) {
                        val device = 1 shl i
                        if (device and remainingDevices == 0) {
                            i++
                            continue
                        }
                        remainingDevices = remainingDevices and device.inv()
                        val name = XposedHelpers.callMethod(param.thisObject, "getSettingNameForDevice", device) as String
                        val mContentResolver = XposedHelpers.getObjectField(XposedHelpers.getSurroundingThis(param.thisObject), "mContentResolver")
                        val index = XposedHelpers.callStaticMethod(Settings.System::class.java, "getIntForUser", mContentResolver, name, if (device == DEVICE_OUT_DEFAULT) DEFAULT_STREAM_VOLUME[mStreamType] else -1, -2) as Int
                        if (index != -1) {
                            val mIndexMap = XposedHelpers.getObjectField(param.thisObject, "mIndexMap") as SparseIntArray
                            mIndexMap.put(device, (XposedHelpers.callMethod(param.thisObject, "getValidIndex", 10 * index) as Int))
                            XposedHelpers.setObjectField(param.thisObject, "mIndexMap", mIndexMap)
                        }
                        i++
                    }
                }
                param.result = null
            }
        })
        lpp.findAndHook("com.android.server.audio.AudioService", "shouldZenMuteStream", Int::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mStreamType = param.args[0] as Int
                if (mStreamType == 5 && !(param.result as Boolean)) {
                    val mZenMode = XposedHelpers.callMethod(XposedHelpers.getObjectField(param.thisObject, "mNm"), "getZenMode") as Int
                    if (mZenMode == 1) param.result = true
                }
            }
        })
    }*/

    internal val luxRef = arrayListOf<TextView>()
    internal val luxPendingMap = mutableMapOf<Int, TextView?>()
    internal var savedNightMode: Boolean? = null

    fun brightnessLuxHook(lpp: LoadPackageParam) {
        val hook: Helpers.MethodHook = object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val context = param.args[0] as Context
                val container = param.thisObject as ViewGroup

                val nightMode = Helpers.isNightMode(context)
                if (savedNightMode == null) {
                    savedNightMode = nightMode
                } else if (savedNightMode != nightMode) {
                    savedNightMode = nightMode
                    luxRef.forEach { v ->
                        (v.parent as? ViewGroup)?.removeView(v)
                    }
                    luxRef.clear()
                    luxPendingMap.values.forEach { v ->
                        (v?.parent as? ViewGroup)?.removeView(v)
                    }
                    luxPendingMap.clear()
                }

                val id = context.resources.getIdentifier("brightness_slider", "id", lpp.packageName)
                val port = context.resources.getIdentifier("qs_brightness", "id", lpp.packageName)
                val land = context.resources.getIdentifier("qs_brightness_land", "id", lpp.packageName)
                if (container.id != id && container.id != port && container.id != land) {
                    return
                }
                if (luxPendingMap[container.id] != null) {
                    return
                }
                if (luxRef.any { it.parent == container }) {
                    return
                }
                val clux = TextView(context)
                val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                lp.alignWithParent = true
                lp.addRule(RelativeLayout.CENTER_IN_PARENT)
                clux.layoutParams = lp
                clux.gravity = Gravity.CENTER
                val isAlt: Boolean = XposedMod.prefs.getBooleanPref("system_showlux_style")
                setTextColors(context, clux)
                if (isAlt) {
                    clux.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
                    clux.alpha = 0.33f
                }
                luxPendingMap[container.id] = clux
                Handler(Looper.getMainLooper()).post {
                    luxPendingMap[container.id] = null
                    container.addView(clux, lp)
                    luxRef.add(clux)
                }
            }
        }
        lpp.findAndHookAllConstructors("com.android.systemui.settings.ToggleSliderView", hook)
        lpp.findAndHookAllConstructors("com.android.systemui.controlcenter.phone.widget.QCToggleSliderView", hook)

        lpp.findAndHook("com.android.systemui.statusbar.phone.StatusBar", "makeStatusBarView", "com.android.internal.statusbar.RegisterStatusBarResult", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mLuxListener = LuxListener()
                XposedHelpers.setAdditionalInstanceField(param.thisObject, "mLuxListener", mLuxListener)
                XposedHelpers.setAdditionalInstanceField(param.thisObject, "mListenerEnabled", false)
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.phone.StatusBar", "setPanelExpanded", Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val isExpanded = param.args[0] as Boolean
                val mListenerEnabled = XposedHelpers.getAdditionalInstanceField(param.thisObject, "mListenerEnabled") as Boolean
                if (isExpanded && !mListenerEnabled)
                    registerLuxListener(param.thisObject)
                else if (!isExpanded && mListenerEnabled)
                    unregisterLuxListener(param.thisObject)
            }
        })

        lpp.findAndHook("com.android.systemui.controlcenter.phone.widget.ControlCenterBrightnessView", "setListening", Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val isExpanded = param.args[0] as Boolean
                val mContext = XposedHelpers.getObjectField(param.thisObject, "mContext") as Context
                val mApplication = mContext.applicationContext // XposedHelpers.callMethod(mContext.applicationContext, "getSystemUIApplication")
                val mStatusBar = XposedHelpers.callMethod(mApplication, "getComponent", XposedHelpers.findClassIfExists("com.android.systemui.statusbar.phone.StatusBar", lpp.classLoader))
                val mListenerEnabled = XposedHelpers.getAdditionalInstanceField(mStatusBar, "mListenerEnabled") as Boolean
                if (isExpanded && !mListenerEnabled)
                    registerLuxListener(mStatusBar)
                else if (!isExpanded && mListenerEnabled)
                    unregisterLuxListener(mStatusBar)
            }
        })
        lpp.findAndHook("com.android.systemui.controlcenter.phone.widget.ControlCenterBrightnessView", "updateResources", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mContext = XposedHelpers.getObjectField(param.thisObject, "mContext") as Context
                luxRef.forEach {
                    setTextColors(mContext, it)
                }
            }
        })

        lpp.findAndHookAllConstructors("com.android.systemui.statusbar.phone.StatusBar", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mScrimController = XposedHelpers.getObjectField(param.thisObject, "mScrimController")
                XposedHelpers.setAdditionalInstanceField(mScrimController, "StatusBar", param.thisObject)
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.phone.ScrimController", "onScreenTurnedOff", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val statusBar = XposedHelpers.getAdditionalInstanceField(param.thisObject, "StatusBar")
                if (statusBar != null) {
                    unregisterLuxListener(statusBar)
                }
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.phone.ScrimController", "onScreenTurnedOn", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val statusBar = XposedHelpers.getAdditionalInstanceField(param.thisObject, "StatusBar")
                if (statusBar != null) {
                    val mPanelExpanded = XposedHelpers.getBooleanField(statusBar, "mPanelExpanded")
                    val mListenerEnabled = XposedHelpers.getAdditionalInstanceField(statusBar, "mListenerEnabled") as Boolean
                    if (mPanelExpanded && !mListenerEnabled) {
                        registerLuxListener(statusBar)
                    }
                }
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.phone.StatusBar","onBootCompleted", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mPanelExpanded = XposedHelpers.getBooleanField(param.thisObject, "mPanelExpanded")
                val mListenerEnabled = XposedHelpers.getAdditionalInstanceField(param.thisObject, "mListenerEnabled") as Boolean
                if (mPanelExpanded && !mListenerEnabled) {
                    registerLuxListener(param.thisObject)
                }
            }
        })
    }

    private var pctRef = arrayListOf<TextView>()
    private fun initPct(container: ViewGroup, source: Int) {
        val context = container.context
        val res = container.resources
        var pct = pctRef.firstOrNull()
        if (pct == null) {
            pct = TextView(context)
            pctRef.clear()
            pctRef.add(pct)
            pct.tag = "mirrorBrightnessPct"
            pct.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40f)
            pct.gravity = Gravity.CENTER
            pct.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL))
            val density = res.displayMetrics.density
            val lp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT)
            lp.topMargin = (XposedMod.prefs.getIntPref("system_showpct_top", 26) * density).roundToInt()
            lp.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
            pct.setPadding((20 * density).roundToInt(), (10 * density).roundToInt(), (18 * density).roundToInt(), (12 * density).roundToInt())
            pct.layoutParams = lp
        } else {
            val parent = pct.parent as? ViewGroup
            parent?.removeView(pct)
        }
        container.addView(pct)
        pct.tag = source
        val panelResId = res.getIdentifier("panel_round_corner_bg", "drawable", "com.android.systemui")
        pct.background = res.getDrawable(panelResId, context.theme)
        val colorResId = res.getIdentifier("qs_tile_icon_disabled_color", "color", "com.android.systemui")
        pct.setTextColor(res.getColor(colorResId, container.context.theme))
        pct.alpha = 0.0f
        pct.visibility = View.GONE
    }
    fun brightnessPctHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.policy.BrightnessMirrorController", "showMirror", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mStatusBarWindow = XposedHelpers.getObjectField(param.thisObject, "mStatusBarWindow") as? ViewGroup
                if (mStatusBarWindow == null) {
                    Logs.x("BrightnessPctHook: mStatusBarWindow is null")
                    return
                }
                initPct(mStatusBarWindow, 1)
                pctRef.firstOrNull()?.visibility = View.VISIBLE
                pctRef.firstOrNull()?.apply {
                    animate().alpha(1.0f).setDuration(300).start()
                }
            }
        })
        lpp.findAndHook("com.android.systemui.statusbar.policy.BrightnessMirrorController", "hideMirror", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                pctRef.firstOrNull()?.visibility = View.GONE
            }
        })
        lpp.findAndHook("com.android.systemui.controlcenter.qs.tileview.QCBrightnessMirrorController","showMirror", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mControlPanelContentView = XposedHelpers.getObjectField(param.thisObject, "mControlPanelContentView") as? ViewGroup
                if (mControlPanelContentView == null) {
                    Logs.x("BrightnessPctHook: mControlPanelContentView is null")
                    return
                }
                initPct(mControlPanelContentView.parent as ViewGroup, 1)
                pctRef.firstOrNull()?.visibility = View.VISIBLE
                pctRef.firstOrNull()?.apply {
                    animate().alpha(1.0f).setDuration(300).start()
                }
            }
        })
        lpp.findAndHook("com.android.systemui.controlcenter.qs.tileview.QCBrightnessMirrorController", "hideMirror", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                pctRef.firstOrNull()?.visibility = View.GONE
            }
        })
        lpp.findAndHookAll("com.android.systemui.settings.BrightnessController", "onChanged", object : Helpers.MethodHook() {
            @SuppressLint("SetTextI18n")
            override fun after(param: MethodHookParam) {
                val pct = pctRef.firstOrNull()
                if (pct == null || (pct.tag as? Int) != 1) return
                val currentLevel = (if (param.args[2] is Int) param.args[2] else param.args[3]) as Int
                val mContext = XposedHelpers.getObjectField(param.thisObject, "mContext") as Context
                val maxLevel = mContext.resources.getInteger(mContext.resources.getIdentifier("config_screenBrightnessSettingMaximum", "integer", "android"))
                pct.text = if (maxLevel > 0) {
                    (currentLevel * 100 / maxLevel).toString() + "%"
                } else (currentLevel * 100).toString() + "%"
            }
        })
    }

    private var statusBarClockId: Int? = null
    fun clockSecondsHook(lpp: LoadPackageParam, clockSecondsSync: Int) {
        fun isStatusBarClockId(clock: TextView) = clock.id == (statusBarClockId
            ?: clock.resources.getIdentifier("clock", "id", "com.android.systemui").also {
                statusBarClockId = it
            })

        fun putSecondsIn(clockChr: CharSequence, calendar: Calendar): String {
            val df: NumberFormat = DecimalFormat("00")
            val clockStr = clockChr.toString()
            val clockStrLower = clockStr.lowercase()
            val colons = clockStr.length - clockStr.replace(":", "").length
            if (colons >= 2) return clockStr
            calendar.timeInMillis = System.currentTimeMillis()
            return if (clockStrLower.endsWith("am") || clockStrLower.endsWith("pm")) {
                clockStr.replace("(?i)(\\s?)(am|pm)".toRegex(), ":" + df.format(calendar[Calendar.SECOND].toLong()) + "$1$2").trim { it <= ' ' }
            } else {
                clockStr.trim { it <= ' ' } + ":" + df.format(calendar[Calendar.SECOND].toLong())
            }
        }

        lpp.findAndHook("com.android.systemui.statusbar.policy.MiuiClock", "updateClock", object : Helpers.MethodHook(XCallback.PRIORITY_HIGHEST) {
            private val calendar = GregorianCalendar()
            override fun before(param: MethodHookParam) {
                val clock = param.thisObject as? TextView ?: return
                if (!isStatusBarClockId(clock)) return
                val now = SystemClock.elapsedRealtime()
                val last = XposedHelpers.getAdditionalInstanceField(clock, "mLastUpdateTime") as? Long
                if (last == null || now - last > 900) {
                    XposedHelpers.setAdditionalInstanceField(param.thisObject, "mLastUpdateTime", now)
                } else {
                    param.result = null
                }
            }

            override fun after(param: MethodHookParam) {
                val clock = param.thisObject as? TextView ?: return
                if (!isStatusBarClockId(clock)) return
                clock.text = putSecondsIn(clock.text, calendar)
            }
        })

        lpp.findAndHookAllConstructors("com.android.systemui.statusbar.policy.MiuiClock", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val clock = param.thisObject as? TextView ?: return
                if (!isStatusBarClockId(clock)) return
                if (XposedHelpers.getAdditionalInstanceField(clock, "mSecondsTimer") != null) return
                val clockHandler = Handler(clock.context.mainLooper)
                var delay = 1000 - SystemClock.elapsedRealtime() % 1000
                delay += if (clockSecondsSync == 1) 50 else 950
                val timer = Timer()
                timer.scheduleAtFixedRate(object : TimerTask() {
                    override fun run() {
                        clockHandler.post { XposedHelpers.callMethod(clock, "updateClock") }
                    }
                }, delay, 1000)
                XposedHelpers.setAdditionalInstanceField(clock, "mSecondsTimer", timer)
            }
        })
    }

    fun hideLockScreenStatusBarHook(lpp: LoadPackageParam) {
        lpp.find("com.android.systemui.statusbar.phone.KeyguardStatusBarView") { clazz ->
            ReflectUtils.findMethodsForClass(clazz, true)?.let { methods ->
                methods.firstOrNull { it.name == "animateNextLayoutChange" }?.let {
                    XposedBridge.hookMethod(it, object : Helpers.MethodHook() {
                        override fun before(param: MethodHookParam) {
                            param.result = null
                        }
                    })
                }
                methods.firstOrNull { it.name == "updateKeyguardStatusBarHeight" }?.let {
                    XposedBridge.hookMethod(it, object : Helpers.MethodHook() {
                        override fun before(param: MethodHookParam) {
                            val statusBarView = param.thisObject as? View ?: return
                            (statusBarView.layoutParams as? MarginLayoutParams)?.let {
                                it.height = 0
                                statusBarView.layoutParams = it
                                param.result = null
                            }
                        }
                    })
                }
            }
        }
    }

    fun hideLockScreenClockHook(lpp: LoadPackageParam) {
        lpp.find("com.android.keyguard.clock.MiuiKeyguardSingleClock") { clazz ->
            XposedBridge.hookAllConstructors(clazz, object : Helpers.MethodHook() {
                override fun after(param: MethodHookParam) {
                    (param.thisObject as? View)?.alpha = 0f
                }
            })
        }
        lpp.find("com.android.keyguard.clock.MiuiKeyguardDualClock") { clazz ->
            XposedBridge.hookAllConstructors(clazz, object : Helpers.MethodHook() {
                override fun after(param: MethodHookParam) {
                    (param.thisObject as? View)?.alpha = 0f
                }
            })
        }
    }

    fun network4GtoLTEHook(lpp: LoadPackageParam) {
        lpp.findAndHook("android.app.MiuiStatusBarManager", "isShowLTEFor4G", Context::class.java, object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                param.result = true
            }
        })
    }

    fun hideLockScreenHintHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.KeyguardIndicationController", "setVisible", Boolean::class.javaPrimitiveType, object : Helpers.MethodHook() {
            override fun before(param: MethodHookParam) {
                if (param.args.size == 1)
                    param.args[0] = false
            }
        })
    }

    fun netSpeedIntervalHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.views.NetworkSpeedView", "onAttachedToWindow", object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val mContext = XposedHelpers.getObjectField(param.thisObject, "mContext") as? Context ?: return
                Settings.System.putInt(mContext.contentResolver, "status_bar_network_speed_interval", XposedMod.prefs.getIntPref("system_netspeedinterval", 4) * 1000)
            }
        })
    }

    fun betterPopupsCenteredHook(lpp: LoadPackageParam) {
        lpp.findAndHook("com.android.systemui.statusbar.notification.stack.StackScrollAlgorithm", "initConstants", Context::class.java, object : Helpers.MethodHook() {
            override fun after(param: MethodHookParam) {
                val context = param.args[0] as? Context ?: return
                val res = context.resources
                var maxPopupHeight = res.getDimensionPixelSize(res.getIdentifier("notification_max_heads_up_height", "dimen", "com.android.systemui"))
                if (res.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) maxPopupHeight /= 3
                val result = (res.displayMetrics.heightPixels / 2.0f - maxPopupHeight / 2.0f).roundToInt().toFloat()
                safe { XposedHelpers.setObjectField(param.thisObject, "mHeadsUpInset", result) }
            }
        })
    }
}

private class LuxListener : SensorEventListener {
    override fun onSensorChanged(event: SensorEvent) {
        val cTime = System.currentTimeMillis()
        val caption = "${event.values[0].roundToInt()} lux"
        luxRef.forEach { textView ->
            if (textView.isAttachedToWindow) try {
                val last = textView.tag as? Long
                if (last != null && cTime - last < 750) return
                textView.text = caption
                textView.tag = cTime
            } catch (t: Throwable) {
                Logs.x(t)
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) = Unit
}

private fun setTextColors(context: Context, textView: TextView) {
    textView.setTextColor(if (Helpers.isNightMode(context)) -0x222223 else -0x9f9fa0)
    if (Helpers.isNightMode(context))
        textView.setShadowLayer(context.resources.displayMetrics.density, 1f, 1f, -0x11dddddc)
    else textView.setShadowLayer(0f, 0f, 0f, Color.WHITE)
}

private fun registerLuxListener(statusBar: Any) {
    try {
        val mContext = XposedHelpers.getObjectField(statusBar, "mContext") as Context
        val powerMgr = mContext.getSystemService(Context.POWER_SERVICE) as PowerManager
        if (!powerMgr.isInteractive) return
        val sBootCompleted = XposedHelpers.getBooleanField(statusBar, "sBootCompleted")
        if (!sBootCompleted) return
        val sensorMgr = mContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val mLuxListener = XposedHelpers.getAdditionalInstanceField(statusBar, "mLuxListener") as LuxListener
        sensorMgr.registerListener(mLuxListener, sensorMgr.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL)
        XposedHelpers.setAdditionalInstanceField(statusBar, "mListenerEnabled", true)
    } catch (t: Throwable) {
        XposedBridge.log(t)
    }
}

private fun unregisterLuxListener(statusBar: Any) {
    try {
        val mContext = XposedHelpers.getObjectField(statusBar, "mContext") as Context
        val sensorMgr = mContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val mLuxListener = XposedHelpers.getAdditionalInstanceField(statusBar, "mLuxListener") as LuxListener
        sensorMgr.unregisterListener(mLuxListener)
        XposedHelpers.setAdditionalInstanceField(statusBar, "mListenerEnabled", false)
    } catch (t: Throwable) {
        XposedBridge.log(t)
    }
}