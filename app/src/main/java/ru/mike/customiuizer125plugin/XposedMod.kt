package ru.mike.customiuizer125plugin

import android.os.Process
import androidx.annotation.Keep
import de.robv.android.xposed.IXposedHookLoadPackage
import de.robv.android.xposed.XSharedPreferences
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam
import org.lsposed.hiddenapibypass.HiddenApiBypass
import ru.mike.customiuizer125plugin.hooks.Slots
import ru.mike.customiuizer125plugin.hooks.Systems
import ru.mike.customiuizer125plugin.hooks.Views
import ru.mike.customiuizer125plugin.prefs.Prefs
import ru.mike.customiuizer125plugin.utils.*
import ru.mike.customiuizer125plugin.utils.Helpers.systemUIPackage
import java.io.File
import java.util.*

@Keep
class XposedMod : IXposedHookLoadPackage {

    // https://www.freeiconspng.com/img/36
    // https://www.freeiconspng.com/img/39552
    // adb shell am set-debug-app -w --persistent packageName
    // adb shell am clear-debug-app packageName

    // adb -s 5b58b2a1 shell am set-debug-app -w --persistent com.android.systemui
    // adb -s 5b58b2a1 shell am clear-debug-app com.android.systemui

    override fun handleLoadPackage(lpp: LoadPackageParam) {
        if (lpp.packageName == systemUIPackage) {
            HiddenApiBypass.addHiddenApiExemptions("L");

            val hookFooter = Prefs.getHookedBool(null, Const.PREF_QS_FOOTER_FIX, Const.DEF_QS_FOOTER_FIX)
            val hookClockTextSize = Prefs.getHookedBool(null, Const.PREF_CLOCK_TEXT_SIZE, Const.DEF_CLOCK_TEXT_SIZE)

            if (hookFooter) {
                Views.fixQsFooterHeight(lpp)
            }
            if (hookClockTextSize) {
                Views.changeBatteryBatteryTextSizeHook(lpp)
            }

            // ---------- CustoMIUIzer prefs -----------

            if (prefs.getBooleanPref("system_statusbaricons_battery1")) Systems.hideIconsBattery1Hook(lpp)
            if (prefs.getBooleanPref("system_statusbaricons_battery2")) Systems.hideIconsBattery2Hook(lpp)
            if (prefs.getBooleanPref("system_statusbaricons_battery3")) Systems.hideIconsBattery3Hook(lpp)

            if (prefs.getBooleanPref("system_statusbaricons_vpn")) Systems.hideIconsVPNHook(lpp)
            if (prefs.getStringAsIntPref("system_statusbaricons_bluetooth", 1) > 1) Systems.hideIconsBluetoothHook(lpp)

            if (Slots.checkNeedHideSlots()) Systems.hideSlotIconsHook(lpp)

            if (prefs.getBooleanPref("system_notifafterunlock")) Systems.showNotificationsAfterUnlockHook(lpp)
            if (prefs.getBooleanPref("system_clockseconds")) {
                val clockSecondsSync = prefs.getStringAsIntPref("system_clockseconds_sync", 0)
                Systems.clockSecondsHook(lpp, clockSecondsSync)
            }

            if (prefs.getBooleanPref("system_hidelsstatusbar")) Systems.hideLockScreenStatusBarHook(lpp)
            if (prefs.getBooleanPref("system_hidelsclock")) Systems.hideLockScreenClockHook(lpp);
            if (prefs.getBooleanPref("system_4gtolte")) Systems.network4GtoLTEHook(lpp);
            if (prefs.getBooleanPref("system_hidelshint")) Systems.hideLockScreenHintHook(lpp);

            if (prefs.getIntPref("system_netspeedinterval", 4) != 4) Systems.netSpeedIntervalHook(lpp)
            if (prefs.getBooleanPref("system_betterpopups_center")) Systems.betterPopupsCenteredHook(lpp)

            // if (prefs.getBooleanPref("system_separatevolume")) Systems.notificationVolumeServiceHook(lpp)
            // if (prefs.getBooleanPref("system_showlux")) Systems.brightnessLuxHook(lpp)

            if (prefs.getBooleanPref("system_showlux")) Systems.brightnessLuxHook(lpp)
            if (prefs.getBooleanPref("system_showpct")) Systems.brightnessPctHook(lpp)
        }
    }

    companion object {
        val prefs by lazy {
            HashMap<String, Any?>().apply {
                val pref = try {
                    if (XposedBridge.getXposedVersion() >= 93)
                        XSharedPreferences(Helpers.modulePkg, Helpers.prefsName)
                    else
                        XSharedPreferences(File(Helpers.prefsFile))
                } catch (t: Throwable) {
                    XposedBridge.log(t)
                    null
                }
                pref?.makeWorldReadable()

                val allPrefs = pref?.all
                if (allPrefs == null || allPrefs.isEmpty())
                    Logs.x("[UID " + Process.myUid() + "] Cannot read module's SharedPreferences, some mods might not work!")
                else this.putAll(allPrefs as HashMap<String, Any?>)
            }
        }
    }

}