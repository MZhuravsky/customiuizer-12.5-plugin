package ru.mike.customiuizer125plugin.prefs;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Prefs {

    private static IPrefs prefs;

    private static IPrefs getPrefs() {
        if (prefs == null) {
            synchronized (Prefs.class) {
                if (prefs == null) {
                    prefs = new PrefsXml();
                }
            }
        }
        return prefs;
    }

    public static boolean getHookedBool(@Nullable Context hookedContext, @NonNull String key, boolean def) {
        return getPrefs().getHookedBool(hookedContext, key, def);
    }

    public static int getHookedInt(@Nullable Context hookedContext, @NonNull String key, int def) {
        return getPrefs().getHookedInt(hookedContext, key, def);
    }

    public static long getHookedLong(@Nullable Context hookedContext, @NonNull String key, long def) {
        return getPrefs().getHookedLong(hookedContext, key, def);
    }

    public static String getHookedString(@Nullable Context hookedContext, @NonNull String key, @NonNull String def) {
        return getPrefs().getHookedString(hookedContext, key, def);
    }

    public static boolean getBool(@NonNull Context context, @NonNull String key, boolean def) {
        return getPrefs().getBool(context, key, def);
    }

    public static void setBool(@NonNull Context context, @NonNull String key, boolean value) {
        getPrefs().setBool(context, key, value);
    }

    public static void setInt(@NonNull Context context, @NonNull String key, int value) {
        getPrefs().setInt(context, key, value);
    }

    public static int getInt(@NonNull Context context, @NonNull String key, int def) {
        return getPrefs().getInt(context, key, def);
    }

    public static long getLong(@NonNull Context context, @NonNull String key, long def) {
        return getPrefs().getLong(context, key, def);
    }

    public static void setLong(@NonNull Context context, @NonNull String key, long value) {
        getPrefs().setLong(context, key, value);
    }

    public static void setString(@NonNull Context context, @NonNull String key, @NonNull String value) {
        getPrefs().setString(context, key, value);
    }

    public static String getString(@NonNull Context context, @NonNull String key, @NonNull String def) {
        return getPrefs().getString(context, key, def);
    }

}
