package ru.mike.customiuizer125plugin.prefs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Map;

import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import ru.mike.customiuizer125plugin.utils.Helpers;
import ru.mike.customiuizer125plugin.utils.Logs;

@SuppressLint({"ApplySharedPref"})
public class PrefsXml implements IPrefs {

    static final String CP_PACKAGE_NAME = "ru.mike.customiuizer125plugin";
    static final String CP_PREF_FILE_NAME = "main_prefs";

    static final String CP_DATA_PATH = "/data/data";
    static final String CP_PREF_DATA_PATH = CP_DATA_PATH + "/" + CP_PACKAGE_NAME +  "/shared_prefs";
    static final String CP_PREF_DATA_FILE = CP_PREF_DATA_PATH + "/" + CP_PREF_FILE_NAME + ".xml";
    static final String CP_DATA_USER_PATH = "/data/user_de/0";
    static final String CP_PREF_USER_PATH = CP_DATA_USER_PATH + "/" + CP_PACKAGE_NAME + "/shared_prefs";
    static final String CP_PREF_USER_FILE = CP_PREF_USER_PATH + "/" + CP_PREF_FILE_NAME + ".xml";

    private static String prefsPathUser = null;
    
    private static XSharedPreferences hookedPrefs = null;
    @Nullable
    private static XSharedPreferences getHookedPrefs() {
        if (hookedPrefs == null) {
            synchronized (PrefsXml.class) {
                if (hookedPrefs == null) {
                    try {
                        if (XposedBridge.getXposedVersion() >= 93)
                            hookedPrefs = new XSharedPreferences(CP_PACKAGE_NAME , CP_PREF_FILE_NAME);
                        else
                            hookedPrefs = new XSharedPreferences(new File(CP_PREF_DATA_FILE));
                        if (!hookedPrefs.getFile().exists()) {
                            hookedPrefs = new XSharedPreferences(new File(CP_PREF_USER_FILE));
                        }
                        hookedPrefs.makeWorldReadable();
                    } catch (Throwable t) {
                        Logs.INSTANCE.x(t);
                    }
                }
            }
        }
        return hookedPrefs;
    }

    @Override
    public void setBool(@NonNull Context context, @NonNull String key, boolean value) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(key, value).commit();
        setWorldReadable(prefs);
    }

    @Override
    public boolean getBool(@NonNull Context context, @NonNull String key, boolean def) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, def);
    }

    @Override
    public void setInt(@NonNull Context context, @NonNull String key, int value) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        prefs.edit().putInt(key, value).commit();
        setWorldReadable(prefs);
    }

    @Override
    public int getInt(@NonNull Context context, @NonNull String key, int def) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getInt(key, def);
    }

    @Override
    public void setLong(@NonNull Context context, @NonNull String key, long value) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        prefs.edit().putLong(key, value).commit();
        setWorldReadable(prefs);
    }

    @Override
    public long getLong(@NonNull Context context, @NonNull String key, long def) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getLong(key, def);
    }

    @Override
    public void setString(@NonNull Context context, @NonNull String key, @NonNull String value) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).commit();
        setWorldReadable(prefs);
    }

    @NonNull
    @Override
    public String getString(@NonNull Context context, @NonNull String key, @NonNull String def) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        String s = prefs.getString(key, def);
        if (s == null) {
            s = def;
        }
        return s;
    }

    @Override
    public boolean getHookedBool(@Nullable Context context, @NonNull String key, boolean def) {
        SharedPreferences p = getHookedPrefs();
        return p != null ? p.getBoolean(key, def) : def;
    }

    @Override
    public int getHookedInt(@Nullable Context hookedContext, @NonNull String key, int def) {
        SharedPreferences p = getHookedPrefs();
        return p != null ? p.getInt(key, def) : def;
    }

    @Override
    public long getHookedLong(@Nullable Context context, @NonNull String key, long def) {
        SharedPreferences p = getHookedPrefs();
        return p != null ? p.getLong(key, def) : def;
    }

    @NonNull
    @Override
    public String getHookedString(@Nullable Context hookedContext, @NonNull String key, @NonNull String def) {
        SharedPreferences p = getHookedPrefs();
        String s = p != null ? p.getString(key, def) : def;
        if (s == null) {
            s = def;
        }
        return s;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint({"SetWorldReadable", "SetWorldWritable"})
    public static void setWorldReadable(@NonNull SharedPreferences prefs) {
        File dataDir = new File(CP_DATA_USER_PATH + "/" + CP_PACKAGE_NAME);
        File prefsDir = new File(dataDir, "shared_prefs");
        File prefsFile = new File(prefsDir, CP_PREF_FILE_NAME + ".xml");
        if (prefsFile.exists()) {
            File[] arr = new File[]{dataDir, prefsDir, prefsFile};
            for (File file : arr) {
                try {
                    file.setReadable(true, false);
                    file.setExecutable(true, false);
                    file.setWritable(true, false);
                } catch (Exception ignored) {}
            }
        }

        if (!Helpers.INSTANCE.copyFile(getSharedPrefsPath(prefs), CP_PREF_DATA_FILE))
            return;

        dataDir = new File(CP_DATA_PATH + "/" + CP_PACKAGE_NAME);
        prefsDir = new File(dataDir, "shared_prefs");
        prefsFile = new File(prefsDir, CP_PREF_FILE_NAME + ".xml");
        if (prefsFile.exists()) {
            File[] arr = new File[]{dataDir, prefsDir, prefsFile};
            for (File file : arr) {
                try {
                    file.setReadable(true, false);
                    file.setExecutable(true, false);
                    file.setWritable(true, false);
                } catch (Exception ignored) {}
            }
        }
    }

    public static String getSharedPrefsPath(@NonNull SharedPreferences prefs) {
        if (prefsPathUser == null) try {
            Field fFile = prefs.getClass().getDeclaredField("mFile");
            fFile.setAccessible(true);
            prefsPathUser = ((File)fFile.get(prefs)).getParentFile().getAbsolutePath();
            return prefsPathUser;
        } catch (Throwable t) {
            return CP_PREF_USER_PATH;
        } else return prefsPathUser;
    }

    public Map<String, ?> getAll(Context context) {
        SharedPreferences prefs = context.createDeviceProtectedStorageContext().getSharedPreferences(CP_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getAll();
    }
}

