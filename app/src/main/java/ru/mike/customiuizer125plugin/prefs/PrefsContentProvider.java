package ru.mike.customiuizer125plugin.prefs;

import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.mike.customiuizer125plugin.XposedMod;
import ru.mike.customiuizer125plugin.utils.Logs;
import ru.mike.customiuizer125plugin.utils.MultiprocessPreferences;

/**
 * Created by me on 17.11.2019.
 */
public class PrefsContentProvider implements IPrefs {

    @Nullable
    private static Context getModuleContext(@Nullable Context hookContext) {
        if (hookContext != null) {
            try {
                // XposedMod or any class in ru.mike.mapstweaks - not in sub-packages!!
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    return hookContext.createPackageContext(XposedMod.class.getPackage().getName(), Context.CONTEXT_IGNORE_SECURITY)
                            .createDeviceProtectedStorageContext();
                } else {
                    return hookContext.createPackageContext(XposedMod.class.getPackage().getName(), Context.CONTEXT_IGNORE_SECURITY);
                }
            } catch (Exception e) {
                Logs.INSTANCE.x("Cannot load module context");
                Logs.INSTANCE.x(e);
            }
        }
        return null;
    }

    @Override
    public boolean getHookedBool(@Nullable Context hookedContext, @NonNull String key, boolean def) {
        try {
            Context context = getModuleContext(hookedContext);
            return context != null
                    ? getBool(context, key, def)
                    : def;
        } catch (Throwable t) {
            Logs.INSTANCE.x("Cannot load preferences");
            Logs.INSTANCE.x(t);
            return def;
        }
    }

    @Override
    public long getHookedLong(@Nullable Context hookedContext, @NonNull String key, long def) {
        try {
            Context context = getModuleContext(hookedContext);
            return context != null
                    ? getLong(context, key, def)
                    : def;
        } catch (Throwable t) {
            Logs.INSTANCE.x("Cannot load preferences");
            Logs.INSTANCE.x(t);
            return def;
        }
    }

    @Override
    public int getHookedInt(@Nullable Context hookedContext, @NonNull String key, int def) {
        try {
            Context context = getModuleContext(hookedContext);
            return context != null
                    ? getInt(context, key, def)
                    : def;
        } catch (Throwable t) {
            Logs.INSTANCE.x("Cannot load preferences");
            Logs.INSTANCE.x(t);
            return def;
        }
    }

    @Override
    public String getHookedString(@Nullable Context hookedContext, @NonNull String key, @NonNull String def) {
        try {
            Context context = getModuleContext(hookedContext);
            return context != null
                    ? getString(context, key, def)
                    : def;
        } catch (Throwable t) {
            Logs.INSTANCE.x("Cannot load preferences");
            Logs.INSTANCE.x(t);
            return def;
        }
    }

    @Override
    public boolean getBool(@NonNull Context context, @NonNull String key, boolean def) {
        return MultiprocessPreferences.getDefaultSharedPreferences(context).getBoolean(key, def);
    }

    @Override
    public void setBool(@NonNull Context context, @NonNull String key, boolean value) {
        MultiprocessPreferences.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    @Override
    public long getLong(@NonNull Context context, @NonNull String key, long def) {
        return MultiprocessPreferences.getDefaultSharedPreferences(context).getLong(key, def);
    }

    @Override
    public void setLong(@NonNull Context context, @NonNull String key, long value) {
        MultiprocessPreferences.getDefaultSharedPreferences(context).edit().putLong(key, value).apply();
    }

    @Override
    public void setInt(@NonNull Context context, @NonNull String key, int value) {
        MultiprocessPreferences.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
    }

    @Override
    public int getInt(@NonNull Context context, @NonNull String key, int def) {
        return MultiprocessPreferences.getDefaultSharedPreferences(context).getInt(key, def);
    }

    @Override
    public void setString(@NonNull Context context, @NonNull String key, @NonNull String value) {
        MultiprocessPreferences.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    @Override
    public String getString(@NonNull Context context, @NonNull String key, @NonNull String def) {
        return MultiprocessPreferences.getDefaultSharedPreferences(context).getString(key, def);
    }
}
