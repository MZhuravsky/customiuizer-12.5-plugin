package ru.mike.customiuizer125plugin.prefs;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface IPrefs {
    boolean getHookedBool(@Nullable Context hookedContext, @NonNull String key, boolean def);
    int getHookedInt(@Nullable Context hookedContext, @NonNull String key, int def);
    long getHookedLong(@Nullable Context hookedContext, @NonNull String key, long def);
    String getHookedString(@Nullable Context hookedContext, @NonNull String key, @NonNull String def);

    boolean getBool(@NonNull Context context, @NonNull String key, boolean def);
    void setBool(@NonNull Context context, @NonNull String key, boolean value);
    void setInt(@NonNull Context context, @NonNull String key, int value);
    int getInt(@NonNull Context context, @NonNull String key, int def);
    long getLong(@NonNull Context context, @NonNull String key, long def);
    void setLong(@NonNull Context context, @NonNull String key, long value);
    void setString(@NonNull Context context, @NonNull String key, @NonNull String value);
    String getString(@NonNull Context context, @NonNull String key, @NonNull String def);
}
