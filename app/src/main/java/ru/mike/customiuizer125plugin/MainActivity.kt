package ru.mike.customiuizer125plugin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.ImageView
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import ru.mike.customiuizer125plugin.prefs.Prefs
import ru.mike.customiuizer125plugin.utils.Const
import ru.mike.customiuizer125plugin.utils.safe

class MainActivity : AppCompatActivity() {

    private val LINK_4PDA = "https://4pda.to/forum/index.php?showuser=683427"
    private val LINK_CUSTOMIUIZER_4PDA = "https://4pda.to/forum/index.php?showtopic=945275&st=99999999"

    private lateinit var textCredits: TextView
    private lateinit var textCreditsCustomiuizer: TextView

    private lateinit var imageCustomiuizerSettings: ImageView

    private lateinit var cbQsFooterHeightFix: CheckBox
    private lateinit var cbClockFontIncrease: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textCredits =  findViewById(R.id.textCredits)
        textCredits.text = Html.fromHtml(resources.getString(R.string.credits, LINK_4PDA))
        textCredits.movementMethod = LinkMovementMethod.getInstance()

        textCreditsCustomiuizer =  findViewById(R.id.textCreditsCustomiuizer)
        textCreditsCustomiuizer.text = Html.fromHtml(resources.getString(R.string.credits_customiuizer, LINK_CUSTOMIUIZER_4PDA))
        textCreditsCustomiuizer.movementMethod = LinkMovementMethod.getInstance()

        imageCustomiuizerSettings =  findViewById(R.id.imageCustomiuizerSettings)
        cbQsFooterHeightFix = findViewById(R.id.cbQsFooterHeightFix)
        cbClockFontIncrease = findViewById(R.id.cbClockFontIncrease)

        val launchIntent = safe { packageManager.getLaunchIntentForPackage(Const.CUSTOMIUIZER_PACKAGE) }
        with (imageCustomiuizerSettings) {
            if (launchIntent != null) {
                isEnabled = true
                alpha = 1f
                setOnClickListener {
                    startActivity(launchIntent)
                }
            } else {
                isEnabled = false
                alpha = 0.5f
            }
        }

        val qsFooterHeightFix: Boolean = Prefs.getBool(this, Const.PREF_QS_FOOTER_FIX, Const.DEF_QS_FOOTER_FIX)
        cbQsFooterHeightFix.isChecked = qsFooterHeightFix
        cbQsFooterHeightFix.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
            Prefs.setBool(this, Const.PREF_QS_FOOTER_FIX, isChecked)
        }

        val clockFontIncrease: Boolean = Prefs.getBool(this, Const.PREF_CLOCK_TEXT_SIZE, Const.DEF_CLOCK_TEXT_SIZE)
        cbClockFontIncrease.isChecked = clockFontIncrease
        cbClockFontIncrease.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
            Prefs.setBool(this, Const.PREF_CLOCK_TEXT_SIZE, isChecked)
        }

    }
}