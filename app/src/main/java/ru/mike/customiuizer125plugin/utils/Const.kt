package ru.mike.customiuizer125plugin.utils

/**
 * Created by me on 17.11.2019.
 */
object Const {
    const val CUSTOMIUIZER_PACKAGE = "name.mikanoshi.customiuizer"

    const val PREF_QS_FOOTER_FIX = "PREF_QS_FOOTER_FIX"
    const val PREF_CLOCK_TEXT_SIZE = "PREF_CLOCK_TEXT_SIZE"

    const val DEF_QS_FOOTER_FIX = true
    const val DEF_CLOCK_TEXT_SIZE = true

}