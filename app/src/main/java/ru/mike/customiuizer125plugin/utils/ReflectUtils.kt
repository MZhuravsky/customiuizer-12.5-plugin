package ru.mike.customiuizer125plugin.utils

import android.text.TextUtils
import androidx.collection.ArrayMap
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.Method
import java.util.*

object ReflectUtils {

    fun findFieldSafe(fieldName: String, target: Any?, declared: Boolean): Field? {
        try {
            return if (target != null) if (declared) target.javaClass.getDeclaredField(fieldName) else target.javaClass.getField(fieldName) else null
        } catch (ignored: NoSuchFieldException) {
        }
        return null
    }

    fun findFieldByReturnType(returnCanonicalClassName: String, target: Any?): Field? {
        if (target != null) {
            var value: Any?
            for (field in target.javaClass.declaredFields) {
                value = getFromFieldSafe(field, target)
                if (value != null && TextUtils.equals(value.javaClass.canonicalName, returnCanonicalClassName)) {
                    return field
                }
            }
        }
        return null
    }

    fun findFieldForClassByReturnType(returnCanonicalClassName: String, targetClass: Class<*>): Field? {
        var value: Any
        for (field in targetClass.declaredFields) {
            if (returnCanonicalClassName == field.type.canonicalName) {
                return field
            }
        }
        return null
    }

    fun findFieldsByReturnType(returnCanonicalClassName: String, target: Any?, declared: Boolean): List<Field> {
        val result: MutableList<Field> = ArrayList()
        if (target != null) {
            var value: Any?
            for (field in if (declared) target.javaClass.declaredFields else target.javaClass.fields) {
                value = getFromFieldSafe(field, target)
                if (value != null && TextUtils.equals(value.javaClass.canonicalName, returnCanonicalClassName)) {
                    result.add(field)
                }
            }
        }
        return result
    }

    fun getFromFieldByReturnType(returnCanonicalClassName: String, target: Any?): Any? {
        return getFromFieldSafe(findFieldByReturnType(returnCanonicalClassName, target), target)
    }

    fun getFromFieldSafe(fieldName: String, target: Any?, declared: Boolean = true): Any? {
        return getFromFieldSafe(findFieldSafe(fieldName, target, declared), target)
    }

    fun getFromFieldSafe(field: Field?, target: Any?): Any? {
        if (field != null && target != null) {
            try {
                field.isAccessible = true
                return field[target]
            } catch (ignored: Exception) {
            }
        }
        return null
    }

    fun setToFieldSafe(field: Field?, target: Any?, value: Any?): Boolean {
        if (field != null && target != null) {
            try {
                field.isAccessible = true
                field[target] = value
                return true
            } catch (ignored: Exception) {
            }
        }
        return false
    }

    fun findFieldsForClass(targetClass: Class<*>?, declared: Boolean): List<Field>? {
        if (targetClass != null) {
            val result = ArrayList<Field>()
            val fields = if (declared) targetClass.declaredFields else targetClass.fields
            Collections.addAll(result, *fields)
            return result
        }
        return null
    }

    fun invokeMethodIfExists(methodName: String, target: Any?, declared: Boolean, vararg args: Any): Any? {
        val method = findMethodByName(methodName, target, declared)
        return if (method != null) invokeMethodSafe(method, target, *args) else null
    }

    fun invokeMethodSafe(method: Method?, target: Any?, vararg args: Any): Any? {
        if (method != null && target != null) {
            try {
                method.isAccessible = true
                return method.invoke(target, *args)
            } catch (ignored: Exception) {
            }
        }
        return null
    }

    fun findMethodSafe(methodName: String, target: Any?, declared: Boolean, vararg parameterTypes: Class<*>): Method? {
        try {
            return if (target != null) if (declared) target.javaClass.getDeclaredMethod(methodName, *parameterTypes) else target.javaClass.getMethod(methodName, *parameterTypes) else null
        } catch (ignored: NoSuchMethodException) {
        }
        return null
    }

    fun findMethodByName(methodName: String, target: Any?, declared: Boolean): Method? {
        if (target != null) {
            val methods = if (declared) target.javaClass.declaredMethods else target.javaClass.methods
            for (m in methods) {
                if (TextUtils.equals(m.name, methodName)) {
                    return m
                }
            }
        }
        return null
    }

    fun findMethodByNameForClass(methodName: String, targetClass: Class<*>?, declared: Boolean): Method? {
        if (targetClass != null) {
            val methods = if (declared) targetClass.declaredMethods else targetClass.methods
            for (m in methods) {
                if (TextUtils.equals(m.name, methodName)) {
                    return m
                }
            }
        }
        return null
    }

    fun findMethodByNamesForClass(methodNames: Array<String>, targetClass: Class<*>?, declared: Boolean): Method? {
        for (methodName in methodNames) {
            val m = findMethodByNameForClass(methodName, targetClass, declared)
            if (m != null) return m
        }
        return null
    }

    fun findMethodForClassByReturnType(returnCanonicalClassName: String, targetClass: Class<*>?, declared: Boolean): Method? {
        if (targetClass != null) {
            try {
                val methods = if (declared) targetClass.declaredMethods else targetClass.methods
                for (m in methods) {
                    if (TextUtils.equals(returnCanonicalClassName, m.returnType.canonicalName)) {
                        return m
                    }
                }
            } catch (ignored: Exception) {
            }
        }
        return null
    }

    fun findMethodByReturnType(returnCanonicalClassName: String, target: Any?, declared: Boolean): Method? {
        return findMethodForClassByReturnType(returnCanonicalClassName, target!!.javaClass, declared)
    }

    fun findMethodByNameForClassArgs(methodName: String, targetClass: Class<*>?, declared: Boolean, argsCount: Int): Method? {
        if (targetClass != null) {
            val methods = if (declared) targetClass.declaredMethods else targetClass.methods
            for (m in methods) {
                if (TextUtils.equals(m.name, methodName) && m.parameterTypes.size == argsCount) {
                    return m
                }
            }
        }
        return null
    }

    fun findMethodByArgsClassesForClass(targetClass: Class<*>?, declared: Boolean, resultClass: Class<*>?, vararg argClasses: Class<*>): Method? {
        if (targetClass != null) {
            val methods = if (declared) targetClass.declaredMethods else targetClass.methods
            for (m in methods) {
                if (m.parameterTypes.size == argClasses.size) {
                    var param: Class<*>
                    var arg: Class<*>
                    var ok = true
                    for (i in m.parameterTypes.indices) {
                        param = m.parameterTypes[i]
                        if (resultClass != null && m.returnType == resultClass || resultClass == null && m.returnType == Void.TYPE) {
                            arg = argClasses[i]
                            if (param != arg) {
                                ok = false
                                break
                            }
                        } else {
                            ok = false
                        }
                    }
                    if (ok) {
                        return m
                    }
                }
            }
        }
        return null
    }

    fun findMethodsForClass(targetClass: Class<*>?, declared: Boolean): List<Method>? {
        if (targetClass != null) {
            val result = ArrayList<Method>()
            val methods = if (declared) targetClass.declaredMethods else targetClass.methods
            Collections.addAll(result, *methods)
            return result
        }
        return null
    }

    val stringClass: Class<*>
        get() = String::class.java
    val byteArrayClass: Class<*>
        get() = ByteArray::class.java

    fun <T> newInstance(clazz: Class<T>, vararg args: Any?): T {
        val sMethodImpl = getConstructor(clazz, *getParameterTypes(*args)!!)
        return sMethodImpl.newInstance(*args)
    }

    fun <T> newInstanceSafe(clazz: Class<T>, vararg args: Any?): T? {
        try {
            return newInstance(clazz, *args)
        } catch (ignored: Exception) {
        }
        return null
    }

    fun <T> getInstance(className: String, vararg args: Any?): T? {
        val clazz: Class<T>? = getClassByName(className)
        if (clazz != null) {
            try {
                return getInstance(clazz, *args)
            } catch (ignored: Exception) {
            }
        }
        return null
    }

    fun <T> getInstance(clazz: Class<T>, vararg args: Any?): T {
        try {
            val res: T? = invokeStatic(clazz, "getInstance", *args)
            if (res != null) {
                return res
            }
        } catch (ignored: NoSuchMethodException) {
        }
        return newInstance(clazz, *args)
    }

    fun <T> invokeStatic(clazz: Class<*>, methodName: String, vararg args: Any?): T? {
        return try {
            val methodImpl = getMethod(clazz, methodName, *args)
            castOrNull(methodImpl.invoke(clazz, *args))
        } catch (e: NoSuchMethodException) {
            throw e
        }
    }

    operator fun <T> invoke(obj: Any, methodName: String, vararg args: Any?): T? {
        return try {
            val methodImpl = getMethod(obj.javaClass, methodName, *args)
            castOrNull(methodImpl.invoke(obj, *args))
        } catch (e: NoSuchMethodException) {
            throw e
        }
    }

    fun getMethod(clazz: Class<*>, methodName: String, vararg args: Any?): Method {
        if (args.isEmpty()) {
            return clazz.getMethod(methodName)
        }
        val params = getParameterTypes(*args)
        if (params != null) {
            try {
                return clazz.getMethod(methodName, *params)
            } catch (ignore: NoSuchMethodException) {
            }
        }

        val methods = clazz.methods
        for (method in methods) {
            if (TextUtils.equals(method.name, methodName)) {
                val paramTypes = method.parameterTypes
                if (paramTypes.size == params!!.size) {
                    var found = true
                    for (paramIdx in paramTypes.indices) {
                        if (params[paramIdx] != null) {
                            if (!isInstanceOf(params[paramIdx], paramTypes[paramIdx])) {
                                found = false
                                break
                            }
                        }
                    }
                    if (found) {
                        return method
                    }
                }
            }
        }
        throw NoSuchMethodException(methodName + " not found in class " + clazz.name)
    }

    private fun getParameterTypes(vararg args: Any?): Array<Class<*>?>? {
        var parameterTypes: Array<Class<*>?>? = null
        if (args.isNotEmpty()) {
            parameterTypes = arrayOfNulls<Class<*>?>(args.size)
            for (idx in args.indices) {
                parameterTypes[idx] = if (args[idx] != null) args[idx]!!.javaClass else null
            }
        }
        return parameterTypes
    }

    private fun <T> getConstructor(clazz: Class<T>, vararg params: Class<*>?): Constructor<T> {
        if (params.isNotEmpty()) {
            val constructors = clazz.declaredConstructors
            for (constructor in constructors) {
                val paramTypes = constructor.parameterTypes
                if (paramTypes.size == params.size) {
                    var found = true
                    for (paramIdx in paramTypes.indices) {
                        if (!isInstanceOf(params[paramIdx], paramTypes[paramIdx])) {
                            found = false
                            break
                        }
                    }
                    if (found) {
                        return cast(constructor)
                    }
                }
            }
        }
        return clazz.getDeclaredConstructor(*params)
    }

    fun isInstanceOf(clazz: Class<*>?, vararg instanceOfClasses: Class<*>): Boolean {
        if (clazz != null) {
            for (instanceOfClass in instanceOfClasses) {
                if (instanceOfClass.isAssignableFrom(clazz)) {
                    return true
                }
                if (instanceOfClass.isPrimitive) {
                    return isPrimitiveCast(instanceOfClass, clazz)
                }
                if (clazz.isPrimitive) {
                    return isPrimitiveCast(clazz, instanceOfClass)
                }
            }
        }
        return false
    }

    fun <T> cast(`object`: Any): T {
        return `object` as T
    }

    fun <T> cast(`object`: Any, classType: Class<T>): T {
        return `object` as T
    }

    fun <T> castOrNull(`object`: Any?): T? {
        return if (`object` != null) `object` as T else null
    }

    fun <T> getClassByName(className: String): Class<T>? {
        try {
            return Class.forName(className) as Class<T>
        } catch (ignored: ClassNotFoundException) {
        }
        return null
    }

    fun isAssignableFrom(clazz: Class<*>?, vararg fromClasses: Class<*>): Boolean {
        if (clazz != null) {
            for (fromClass in fromClasses) {
                if (clazz.isAssignableFrom(fromClass)) {
                    return true
                }
            }
        }
        return false
    }

    fun isAssignableFrom(className: String?, vararg fromClasses: Class<*>): Boolean {
        return isAssignableFrom(getClassByName<Any>(className!!), *fromClasses)
    }

    fun isInstanceOf(className: String?, vararg instanceOfClasses: Class<*>): Boolean {
        return isInstanceOf(getClassByName<Any>(className!!), *instanceOfClasses)
    }

    fun isInstanceOf(obj: Any?, vararg instanceOfClasses: Class<*>): Boolean {
        return obj != null && isInstanceOf(obj.javaClass, *instanceOfClasses)
    }

    private val primitiveClassesMap: MutableMap<Class<*>?, Class<*>> = ArrayMap(8)
    private fun isPrimitiveCast(primitiveClazz: Class<*>, clazz: Class<*>): Boolean {
        return primitiveClazz == clazz || primitiveClassesMap[primitiveClazz] == clazz
    }

    init {
        primitiveClassesMap[Boolean::class.javaPrimitiveType] = Boolean::class.java
        primitiveClassesMap[Char::class.javaPrimitiveType] = Char::class.java
        primitiveClassesMap[Byte::class.javaPrimitiveType] = Byte::class.java
        primitiveClassesMap[Short::class.javaPrimitiveType] = Short::class.java
        primitiveClassesMap[Int::class.javaPrimitiveType] = Int::class.java
        primitiveClassesMap[Long::class.javaPrimitiveType] = Long::class.java
        primitiveClassesMap[Float::class.javaPrimitiveType] = Float::class.java
        primitiveClassesMap[Double::class.javaPrimitiveType] = Double::class.java
    }
}