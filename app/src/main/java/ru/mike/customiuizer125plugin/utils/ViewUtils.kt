package ru.mike.customiuizer125plugin.utils

import android.R
import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.os.Handler
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.*
import android.view.ViewGroup.MarginLayoutParams
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.animation.DecelerateInterpolator
import android.widget.LinearLayout
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.view.ViewCompat
import de.robv.android.xposed.XposedHelpers
import java.util.*
import java.util.concurrent.atomic.AtomicReference
import kotlin.math.roundToInt

object ViewUtils {

    fun dpToPx(context: Context, dp: Int) = dpToPx(context, dp.toFloat())
    fun dpToPx(context: Context, dp: Float): Int {
        val displayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT.toFloat())).roundToInt()
    }

    fun getActivity(view: View?): Activity? {
        if (view == null) return null
        if (view.context is Activity) return view.context as Activity
        if (view.context is android.view.ContextThemeWrapper) {
            val wrapper1 = view.context as android.view.ContextThemeWrapper
            return if (wrapper1.baseContext is Activity) {
                wrapper1.baseContext as Activity
            } else {
                null
            }
        }
        if (view.context is ContextThemeWrapper) {
            val wrapper2 = view.context as ContextThemeWrapper
            return if (wrapper2.baseContext is Activity) {
                wrapper2.baseContext as Activity
            } else {
                null
            }
        }
        return null
    }

    fun <T : View?> findViewByResName(parentView: ViewGroup, resName: String): T? {
        val listId = parentView.context.resources.getIdentifier(resName, "id", parentView.context.packageName)
        return if (listId != 0) parentView.findViewById(listId) else null
    }

    fun <T : View?> findViewByResName(activity: Activity, resName: String): T? {
        val listId = activity.resources.getIdentifier(resName, "id", activity.packageName)
        if (listId != 0) {
            try {
                val view = activity.findViewById<View>(listId)
                return view as T
            } catch (ignored: Exception) {
            }
        }
        return null
    }

    fun getViewIdByResName(activity: Activity, resName: String): Int {
        return activity.resources.getIdentifier(resName, "id", activity.packageName)
    }

    fun getStringIdByResName(activity: Activity, resName: String): Int {
        return activity.resources.getIdentifier(resName, "string", activity.packageName)
    }

    fun getStringByResName(activity: Activity, resName: String): String? {
        val id = getStringIdByResName(activity, resName)
        return if (id != 0) activity.getString(id) else null
    }

    fun getActivityContent(activity: Activity): View {
        return activity.findViewById(R.id.content)
    }

    fun getActivityPosition(activity: Activity): Point {
        val content = getActivityContent(activity)
        val loc = IntArray(2)
        content.getLocationOnScreen(loc)
        return Point(loc[0], loc[1])
    }

    fun getDisplaySize(activity: Activity): Point {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    fun getMarginBottom(view: View): Int {
        val lp = view.layoutParams
        return if (lp is MarginLayoutParams) lp.bottomMargin else 0
    }

    fun setMarginBottom(view: View, marginBottom: Int) {
        val lp = view.layoutParams
        if (lp is MarginLayoutParams) lp.bottomMargin = marginBottom
    }

    fun strToInt(s: String, def: Int): Int {
        if (!TextUtils.isEmpty(s)) {
            try {
                return s.toInt()
            } catch (ignored: NumberFormatException) {
            }
        }
        return def
    }

    fun getActivityHandler(activity: Activity): Handler? {
        try {
            val handler = XposedHelpers.getObjectField(activity, "mHandler")
            if (handler is Handler) return handler
        } catch (ignored: Exception) {
        }
        return null
    }

    fun tryPost(view: View, runnable: Runnable) {
        val activity = getActivity(view)
        val handler = if (activity != null) getActivityHandler(activity) else null
        handler?.post(runnable)
    }

    fun tryPostDelayed(view: View, runnable: Runnable, delayMs: Long) {
        val activity = getActivity(view)
        val handler = if (activity != null) getActivityHandler(activity) else null
        handler?.postDelayed(runnable, delayMs)
    }

    fun <T> getClassByName(className: String): Class<T>? {
        try {
            return Class.forName(className) as Class<T>
        } catch (ignored: Exception) {
        }
        return null
    }

    fun <T : View?> findViewByClass(activity: Activity, clazz: Class<T>): T? {
        return findViewByClass(getRootView(activity), clazz)
    }

    fun <T : View?> findViewByClass(viewGroup: ViewGroup, clazz: Class<T>): T? {
        var view: View?
        for (i in 0 until viewGroup.childCount) {
            view = viewGroup.getChildAt(i)
            if (clazz.isAssignableFrom(view.javaClass)) {
                return view as T?
            } else if (view is ViewGroup) {
                view = findViewByClass(view, clazz)
                if (view != null) {
                    return view
                }
            }
        }
        return null
    }

    fun <T> findViewsByClass(activity: Activity, clazz: Class<T>): List<T> {
        return findViewsByClass(getRootView(activity), clazz)
    }

    fun <T> findViewsByClass(viewGroup: ViewGroup, clazz: Class<T>): List<T> {
        val list: MutableList<T> = ArrayList()
        var view: View
        for (i in 0 until viewGroup.childCount) {
            view = viewGroup.getChildAt(i)
            if (clazz.isAssignableFrom(view.javaClass)) {
                list.add(view as T)
            } else if (view is ViewGroup) {
                list.addAll(findViewsByClass(view, clazz))
            }
        }
        return list
    }

    fun getContentView(activity: Activity): ViewGroup {
        return activity.findViewById<View>(R.id.content) as ViewGroup
    }

    fun getRootView(activity: Activity): ViewGroup {
        return getContentView(activity).rootView as ViewGroup
    }

    fun getLocationOnScreen(view: View): Point {
        val scrXY = IntArray(2)
        view.getLocationOnScreen(scrXY)
        val resX = scrXY[0]
        val resY = scrXY[1]
        return Point(resX, resY)
    }

    fun runOnViewSize(view: View, runnable: Runnable) {
        runOnViewSize(view, object : IViewRunnable<View> {
            override fun onView(view: View) = runnable.run()
        })
    }

    fun <T : View?> runOnViewSize(view: T, viewRunnable: IViewRunnable<T>) {
        if (view!!.width > 0 && view.height > 0) {
            viewRunnable.onView(view)
            return
        }
        runOnTreeObserver(view) { viewRunnable.onView(view) }
    }

    private fun runOnTreeObserver(view: View, runnable: Runnable) {
        val vto = view.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                runnable.run()
                removeGlobalLayoutListener(view, this)
            }
        })
    }

    fun removeGlobalLayoutListener(view: View, listenerRef: AtomicReference<OnGlobalLayoutListener?>?) {
        if (listenerRef != null) {
            removeGlobalLayoutListener(view, listenerRef.get())
            listenerRef.set(null)
        }
    }

    fun removeGlobalLayoutListener(view: View, listener: OnGlobalLayoutListener?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        } else {
            view.viewTreeObserver.removeGlobalOnLayoutListener(listener)
        }
    }

    fun animateHorz(view: View, toTranslationX: Float, duration: Long, delay: Long) {
        val animator = ViewCompat.animate(view)
        animator.translationX(toTranslationX)
        animator.interpolator = DecelerateInterpolator()
        if (duration > 0) animator.duration = duration
        if (delay > 0) animator.startDelay = delay
        animator.start()
    }

    fun detach(view: View) {
        val p = view.parent
        if (p is ViewGroup) {
            p.removeView(view)
        }
    }

    fun withView(view: View?, runnable: IViewRunnable<View?>) {
        if (view != null) runnable.onView(view)
    }

    fun scale(view: View, scale: Float) {
        view.scaleX = scale
        view.scaleY = scale
    }

    fun setWeight(view: View, weight: Float) {
        val lp = view.layoutParams
        if (lp is LinearLayout.LayoutParams) {
            lp.width = 0
            lp.weight = weight
        }
    }

    fun setHeight(view: View, height: Int) {
        val lp = view.layoutParams
        if (lp.height != height) {
            lp.height = height
            view.layoutParams = lp
        }
    }

    interface IViewRunnable<T : View?> {
        fun onView(view: T)
    }
}