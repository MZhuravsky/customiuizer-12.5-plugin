package ru.mike.customiuizer125plugin.utils

import androidx.annotation.IntegerRes

inline infix fun <reified R> Any?.azz(clazz: Class<R>) =
    if (this != null) clazz.cast(this) else null

inline fun <reified R> Any?.castTo(): R? {
    return if (this is R) {
        this
    } else null
}

inline fun <reified R> Any?.runAs(block: (obj: R) -> Unit): R? {
    return if (this is R) {
        block.invoke(this as R)
        this
    } else null
}

inline fun <reified T> Any?.withAs(block: T.() -> Unit) {
    if (this is T) {
        return this.block()
    }
}

inline fun <reified T, R> Any?.letAs(def: R, block: T.() -> R): R {
    return if (this is T) {
        return this.block()
    } else def
}

fun CharSequence?.isNotNullOrBlank() = this != null && !this.isBlank()

inline fun <reified T> Any.updateFields(block: (name: String) -> T?) = this.updateFields({ true }, block)

inline fun <reified T> Any.updateFields(filter: (name: String) -> Boolean, block: (name: String) -> T?) {
    val fields = this::class.java.declaredFields.filter { T::class.java.isAssignableFrom(it.type) }.filter { filter.invoke(it.name) }
    for (idx in fields.indices) {
        val field = fields[idx]
        val newValue = block.invoke(field.name)
        if (newValue != null)
            try {
                field.isAccessible = true
                field.set(this, newValue)
            } catch (ignored: Exception) {}
    }
}

inline fun <reified T> Any.checkFieldsOk() = this.checkFieldsOk<T> { true }

inline fun <reified T> Any.checkFieldsOk(filter: (name: String) -> Boolean): Boolean {
    val fields = this::class.java.declaredFields.filter { T::class.java.isAssignableFrom(it.type) }.filter { filter.invoke(it.name) }
    for (idx in fields.indices) {
        val field = fields[idx]
        val value = try {
            field.get(this)
        } catch (e: Exception) {
            null
        }
        val isBad = value == null
        if (isBad)
            return false
    }
    return true
}

fun String?.hasLetters(): Boolean {
    this?.forEach { c -> if (c.isLetter()) return true }
    return false
}

fun <T> safe(block: () -> T?): T? {
    return try {
        block.invoke()
    } catch (e: Throwable) {
        null
    }
}

fun HashMap<String, Any?>.getStringPref(key: String, def: String? = null): String? = this.getOrDefault("pref_key_$key", def)?.toString() ?: def
fun HashMap<String, Any?>.getStringAsIntPref(key: String, def: Int): Int = safe { this.getStringPref(key)?.toInt() } ?: def
fun HashMap<String, Any?>.getBooleanPref(key: String, def: Boolean = false): Boolean = safe { this.getStringPref(key)?.toBoolean() } ?: def
fun HashMap<String, Any?>.getIntPref(key: String, def: Int = 0): Int = safe { this["pref_key_$key"] as? Int } ?: def
