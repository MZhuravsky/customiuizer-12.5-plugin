package ru.mike.customiuizer125plugin.utils

import android.content.Context
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers
import de.robv.android.xposed.callbacks.XC_LoadPackage
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

// const val DEBUG = false
// XposedBridge.invokeOriginalMethod(param.method, param.thisObject, param.args)

fun waitForContext(lpparam: XC_LoadPackage.LoadPackageParam, block: (XC_LoadPackage.LoadPackageParam, Context) -> Unit) {
    val attachHookRef = AtomicReference<XC_MethodHook.Unhook>(null)
    val isHooked = AtomicBoolean(false)
    val attachHook = XposedHelpers.findAndHookMethod(XposedHelpers.findClass("android.content.ContextWrapper", lpparam.classLoader),
                                                     "attachBaseContext", Context::class.java, object : XC_MethodHook() {
            override fun afterHookedMethod(param: MethodHookParam) {
                param.thisObject?.takeIf { it is Context }?.let { ctx ->
                    val context = ctx as Context
                    val moduleEnabled = true // Prefs.getHookedBool(context, Consts.PREF_MODULE_ENABLE, Consts.DEF_MODULE_ENABLE)
                    if (moduleEnabled) {
                        if (isHooked.compareAndSet(false, true)) {
                            block.invoke(lpparam, context)
                        }
                    } else {
                        unhookRef(attachHookRef)
                    }
                }
            }
        })
    attachHookRef.set(attachHook)
}

fun waitForDex(lpparam: XC_LoadPackage.LoadPackageParam, block: (XC_LoadPackage.LoadPackageParam, Context) -> Unit) {
    val attachHookRef = AtomicReference<XC_MethodHook.Unhook>(null)
    val attachHook = XposedHelpers.findAndHookMethod(XposedHelpers.findClass("android.app.Application", lpparam.classLoader),
                                                     "attach", Context::class.java, object : XC_MethodHook() {
            override fun afterHookedMethod(param: MethodHookParam) {
                param.thisObject?.takeIf { it is Context }?.let { ctx ->
                    val context = ctx as Context
                    val moduleEnabled = true // Prefs.getHookedBool(context, Consts.PREF_MODULE_ENABLE, Consts.DEF_MODULE_ENABLE)
                    if (moduleEnabled) {
                        block.invoke(lpparam, context)
                    } else {
                        unhookRef(attachHookRef)
                    }
                }
            }
        })
    attachHookRef.set(attachHook)
}

fun unhookRef(unhookRef: AtomicReference<XC_MethodHook.Unhook>?) {
    unhookRef?.run {
        get()?.unhook()
        set(null)
    }
}

fun unhookRefs(unhookRef: AtomicReference<Set<XC_MethodHook.Unhook>>?) {
    unhookRef?.run {
        get()?.filterNotNull()?.forEach {
            it.unhook()
        }
        set(null)
    }
}

fun ClassLoader.find(clazzName: String): Class<*>? {
    val clazz = XposedHelpers.findClassIfExists(clazzName, this)
    if (clazz == null) {
        Logs.x("Cannot find class $clazzName")
    }
    return clazz
}

fun ClassLoader.find(clazzName: String, block: (Class<*>) -> Unit) {
    XposedHelpers.findClassIfExists(clazzName, this)?.let { block.invoke(it) }
        ?: kotlin.run {
            Logs.x("Cannot find class $clazzName")
        }
}

fun XC_LoadPackage.LoadPackageParam.find(clazzName: String) = this.classLoader.find(clazzName)
fun XC_LoadPackage.LoadPackageParam.find(clazzName: String, block: (Class<*>) -> Unit) = this.classLoader.find(clazzName, block)

fun ClassLoader.findAndHook(className: String, methodName: String, vararg parameterTypesAndCallback: Any?): XC_MethodHook.Unhook? =
    safe { XposedHelpers.findAndHookMethod(className, this, methodName, *parameterTypesAndCallback) }
        ?: kotlin.run {
            Logs.x("Failed to hook $methodName method in $className")
            null
        }

fun ClassLoader.findAndHookAll(className: String, methodName: String, callback: XC_MethodHook?): Set<XC_MethodHook.Unhook>? =
    safe { XposedBridge.hookAllMethods(this.find(className), methodName, callback) }
        ?: kotlin.run {
            Logs.x("Failed to hook $methodName methods in $className")
            null
        }

fun XC_LoadPackage.LoadPackageParam.findAndHook(className: String, methodName: String, vararg parameterTypesAndCallback: Any?) =
    this.classLoader.findAndHook(className, methodName, *parameterTypesAndCallback)

fun XC_LoadPackage.LoadPackageParam.findAndHookAll(className: String, methodName: String, callback: XC_MethodHook?): Set<XC_MethodHook.Unhook>? =
    this.classLoader.findAndHookAll(className, methodName, callback)

fun ClassLoader.findAndHookConstructor(className: String, vararg parameterTypesAndCallback: Any?) =
    safe { XposedHelpers.findAndHookConstructor(className, this, *parameterTypesAndCallback) }
        ?: kotlin.run {
            Logs.x("Failed to hook constructor with parameters \"${parameterTypesAndCallback.toList().map { it.toString() }}\" for $className")
            null
        }

fun XC_LoadPackage.LoadPackageParam.findAndHookConstructor(className: String, vararg parameterTypesAndCallback: Any?) =
    this.classLoader.findAndHookConstructor(className, *parameterTypesAndCallback)

fun ClassLoader.findAndHookAllConstructors(className: String, callback: XC_MethodHook): Set<XC_MethodHook.Unhook>? =
    this.find(className)?.let {
        safe { XposedBridge.hookAllConstructors(it, callback) }
            ?: kotlin.run {
                Logs.x("Failed to hook all constructors for $className")
                null
            }
    }

fun XC_LoadPackage.LoadPackageParam.findAndHookAllConstructors(className: String, callback: XC_MethodHook) =
    this.classLoader.findAndHookAllConstructors(className, callback)