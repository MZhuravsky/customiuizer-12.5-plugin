package ru.mike.customiuizer125plugin.utils

import de.robv.android.xposed.XposedHelpers
import java.lang.reflect.Field

fun <T> Any?.getFromFieldSafe(fieldName: String): T? = XposedHelpers.getObjectField(this, fieldName) as? T
fun <T> Any?.getFromFieldsSafe(vararg fieldNames: String): List<T> {
    val result = ArrayList<T>()
    for (fieldName in fieldNames) {
        (XposedHelpers.getObjectField(this, fieldName) as? T)?.let {
            result.add(it)
        }
    }
    return result
}

fun <T> Any?.getFromFieldSafe(field: Field?): T? = ReflectUtils.getFromFieldSafe(field, this) as? T