package ru.mike.customiuizer125plugin.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import de.robv.android.xposed.XC_MethodHook
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.reflect.Field
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

object Helpers {

    const val systemUIPackage = "com.android.systemui"

    const val moduleModPkg = "org.strawing.customiuizermod"
    const val modulePkg = moduleModPkg // "name.mikanoshi.customiuizer"
    const val prefsName = "customiuizer_prefs"
    const val prefsPath = "/data/user_de/0/$modulePkg/shared_prefs"
    const val prefsFile = "$prefsPath/$prefsName.xml"

    open class MethodHook : XC_MethodHook {
        open fun before(param: MethodHookParam) = Unit
        open fun after(param: MethodHookParam) = Unit

        constructor() : super()
        constructor(priority: Int) : super(priority)

        public override fun beforeHookedMethod(param: MethodHookParam?) {
            try {
                param?.let { before(it) }
            } catch (t: Throwable) {
                Logs.x(t)
            }
        }

        public override fun afterHookedMethod(param: MethodHookParam?) {
            try {
                param?.let { after(it) }
            } catch (t: Throwable) {
                Logs.x(t)
            }
        }
    }

    /*@Synchronized
    fun getModuleRes(context: Context): Resources {
        val config = context.resources.configuration
        val moduleContext: Context = getModuleContext(context)
        return if (config == null) moduleContext.resources else moduleContext.createConfigurationContext(config).resources
    }

    @Synchronized
    fun getModuleContext(context: Context) = getModuleContext(context, null)

    @Synchronized
    fun getModuleContext(context: Context, config: Configuration?): Context =
        context.createPackageContext(modulePkg, Context.CONTEXT_IGNORE_SECURITY).createDeviceProtectedStorageContext().let { moduleContext ->
            if (config == null) moduleContext else moduleContext.createConfigurationContext(config)
        }
    */

    fun isNightMode(context: Context) =
        context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES

    fun copyFile(from: String?, to: String?): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Files.copy(Paths.get(from), Paths.get(to), StandardCopyOption.REPLACE_EXISTING)
                return true
            } else FileInputStream(from).use { `in` ->
                FileOutputStream(to, false).use { out ->
                    val buf = ByteArray(1024)
                    var len: Int
                    while (`in`.read(buf).also { len = it } > 0) out.write(buf, 0, len)
                    return true
                }
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            return false
        }
    }

}