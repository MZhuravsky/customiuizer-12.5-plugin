package ru.mike.customiuizer125plugin.utils

import android.view.View
import de.robv.android.xposed.XposedBridge

/**
 * Created by me on 07.11.2019.
 */
object Logs {
    private const val PREFIX = "[CustoMIUIzer 12.5 Plugin] "

    @JvmOverloads
    fun x(s: String, showLogs: Boolean = true) {
        if (showLogs) XposedBridge.log(PREFIX + s)
    }

    fun x(t: Throwable?) {
        XposedBridge.log(t)
    }

    fun writeHierarchy(view: View?) {
        var sb = StringBuilder()
        var v = view
        while (v != null) {
            sb.append("   ------------------> ").append(v.javaClass.simpleName)
            sb.append("; ").append("l=").append(v.left)
            sb.append("; ").append("r=").append(v.right)
            sb.append("; ").append("t=").append(v.top)
            sb.append("; ").append("b=").append(v.bottom)
            sb.append("; ").append("scrX=").append(v.scrollX)
            sb.append("; ").append("scrY=").append(v.scrollY)
            sb.append("; ").append("trX=").append(v.translationX)
            sb.append("; ").append("trY=").append(v.translationY)
            x(sb.toString())
            sb = StringBuilder()
            if (v.parent !is View) break
            v = v.parent as View
        }
    }
}